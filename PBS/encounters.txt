﻿#-------------------------------
[048] # Route 1
Land,17
    40,SWOVE,2,4
    40,SWOVE,2,4
    40,TASWHIRL,2,4
    40,ATTDER,2,3
    5,SLITHAPOINT,2,4
    1,JAYREEZE,2,3
#-------------------------------
[078] # Apricota Forest
Land,17
    40,ATTDER,6,8
    25,SLITHAPOINT,6,8
    25,INGRUDGE,6,8
    5,SKORCHION,8,10
    1,POTIGHOUL,8,10
#-------------------------------
[081] # Route 2
Land,17
    40,INGRUDGE,7,9
    40,SKORCHION,7,9
    25,SLITHAPOINT,7,9
    15,SNAPTILE,7,9
    1,HAWKSTREAM,8,10
#-------------------------------
[086] # Route 3
Land,17
    40,ATTDER,10,12
    40,TASWHIRL,9,11
    25,CHUNINYA,9,12
    15,BRAYD,10,12
    1,CHICKITE,11,12