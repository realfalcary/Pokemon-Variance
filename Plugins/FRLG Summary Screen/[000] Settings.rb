#------------------------------------------------------------------------------
# Settings
#------------------------------------------------------------------------------
# This contains settings to modify the FRLG Summary Screen Plugin 
#------------------------------------------------------------------------------
module Settings
    #--------------------------------------------------------------------------
    # If this setting is set to true, page 2 is modified to display EVs and IVs
    #--------------------------------------------------------------------------
    SUMMARY_EV_IV = true

    #--------------------------------------------------------------------------
    # If this setting is set to true, IV ratings are displayed in page two
    #--------------------------------------------------------------------------
    SUMMARY_IV_RATINGS = true

    #--------------------------------------------------------------------------
    # If this setting is set to true, hidden ability names appear in 
    # different colors
    #--------------------------------------------------------------------------
    SUMMARY_HIDDEN_ABILITY = true
end